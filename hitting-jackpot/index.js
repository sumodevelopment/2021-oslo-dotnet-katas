export function testJackpot(items) {
    return new Set(items).size === 1
}