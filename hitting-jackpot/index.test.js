import { testJackpot } from './index'

describe('testJackpot', function() {

    test('["@", "@", "@", "@"] -> true', function() {
        const source = ["@", "@", "@", "@"]
        expect(testJackpot( source )).toBe( true )
    })
    
    test('["abc", "abc", "abc", "abc"] -> true', function() {
        const source = ["abc", "abc", "abc", "abc"]
        expect(testJackpot( source )).toBe( true )
    })

    test('["SS", "SS", "SS", "SS"] -> true', function() {
        const source = ["SS", "SS", "SS", "SS"]
        expect(testJackpot( source )).toBe( true )
    })

    test('["&&", "&", "&&&", "&&&&"] -> false', function() {
        const source = ["&&", "&", "&&&", "&&&&"]
        expect(testJackpot( source )).toBe( false )
    })

    test('["SS", "SS", "SS", "Ss"] -> false', function() {
        const source = ["SS", "SS", "SS", "Ss"]
        expect(testJackpot( source )).toBe( false )
    })

    
    
   
    
   
    
   
    


})