export function sortWord(word) {

    if (word.length === 0 || typeof word !== 'string') {
        return false
    }

    return word
        .split('')
        .sort()
        .join('')
}
