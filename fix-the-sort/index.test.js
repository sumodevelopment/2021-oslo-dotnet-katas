import { sortWord } from './index'

test('dcba should be abcd', function(){
    const source = 'dcba'
    const expected = 'abcd'
    expect( sortWord( source ) ).toBe( expected )
})

test('UnpredictableA should be AUabcdeeilnprt', function(){
    const source = 'UnpredictableA'
    const expected = 'AUabcdeeilnprt'
    expect( sortWord( source ) ).toBe( expected )
})

test('pneumonoultramicroscopicsilicovolcanoconiosis should be aacccccceiiiiiilllmmnnnnooooooooopprrsssstuuv', function(){
    const source = 'pneumonoultramicroscopicsilicovolcanoconiosis'
    const expected = 'aacccccceiiiiiilllmmnnnnooooooooopprrsssstuuv'
    expect( sortWord( source ) ).toBe( expected )
})

test('if not string should be false', function(){
    const source = ''
    expect( sortWord( source ) ).toBe( false )
})

test('if not string should be false', function() {
    const source = 10
    expect( sortWord( source ) ).toBe( false )
})