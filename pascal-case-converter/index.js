export const removeSpecialCharacters = string => string.replace(/[^a-zA-Z' ]/g, '')
export const capitalize = string => string[0].toUpperCase() + string.slice(1).toLowerCase()
export const sentenceToArray = sentence => sentence.split(' ')

export function convertToPascal( sentence ) {
    return sentenceToArray(
        removeSpecialCharacters( sentence )
    ).reduce((acc, word) => acc + capitalize(word), '');
}