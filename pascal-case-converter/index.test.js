import { removeSpecialCharacters, capitalize, sentenceToArray, convertToPascal } from './index'

describe('Remove punctuation', () => {

    test('remove punctuation !pinea-p.pl?e -> pineapple', () => {
        const source = '!pinea-p.pl?e'
        const result = 'pineapple'
        expect(removeSpecialCharacters(source)).toBe(result)
    })

    test('remove punctuation: #=+java?ScrIp!T -> javaScrIpT', () => {
        const source = '#=+java?ScrIp!T'
        const result = 'javaScrIpT'
        expect(removeSpecialCharacters(source)).toBe(result)
    })

    test('remove punctuation: #=+@ -> ""', () => {
        const source = '#=+java?ScrIp!T'
        const result = 'javaScrIpT'
        expect(removeSpecialCharacters(source)).toBe(result)
    })

})

describe('capitalize word', () => {
    test('capitalize javascript -> Javascript', () => {
        const source = 'javascript'
        const result = 'Javascript'
        expect(capitalize(source)).toBe(result)
    })

    test('capitalize coDiNg -> Coding', () => {
        const source = 'coDiNg'
        const result = 'Coding'
        expect(capitalize(source)).toBe(result)
    })

    test('capitalize WildBiLl -> Wildbill', () => {
        const source = 'WildBiLl'
        const result = 'Wildbill'
        expect(capitalize(source)).toBe(result)
    })
})

describe('split sentence to array', () => {

    test('capitalize it is javascript -> [it, is, javascript]', () => {
        const source = 'it is javascript'
        const result = ['it', 'is', 'javascript']
        expect(sentenceToArray(source)).toEqual(result)
    })

    test('remove spaces i like code -> [i, like, code]', () => {
        const source = 'i like code'
        const result = ['i', 'like', 'code']
        expect(sentenceToArray(source)).toEqual(result)
    })

})

describe('convert to pascal', () => {
    test('convert: i lO@ve javaSc!rip!t -> ILoveJavascript', () => {
        const source = 'i lO@ve javaScript'
        const result = 'ILoveJavascript'
        expect( convertToPascal( source ) ).toBe( result )
    })
})